

all:
	javac HelloJNI.java
	gcc -c -I/usr/lib/jvm/java-7-openjdk-amd64/include/ -fPIC HelloJNI.c
	gcc -shared -o libhello.so HelloJNI.o

run:
	java -Djava.library.path=. HelloJNI

clean:
	rm -f *.o *.so *.class

